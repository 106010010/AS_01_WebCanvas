# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

![](https://i.imgur.com/CkjpC0d.png)

進入頁面後，白色的區域就是作畫的地方，旁邊的藍色區塊則是工具區。除了預設的畫筆之外，還有橡皮擦、文字、圓形、三角形、矩形、上一步、下一步、重置畫布、下載圖畫、上傳圖畫與畫直線，下方一排是可以選取畫筆、圖形與文字的顏色，最右邊那格按下後，會出現一個小視窗可以自己選想要的所有顏色。

![](https://i.imgur.com/5njJDf7.png)

而最上方有調整筆刷、橡皮擦及圖形線條粗細的地方，還有調整文字字形及文字大小的地方。



---



### Function description

我的功能大多與助教規定的相同，多了畫直線的功能，它的實做其實也很簡單，只是在mousedown跟mousemove之間不是一直畫出軌跡，而是把兩端相連。

此外，我的三角形也與助教示範的不同，不會是等腰三角形，而是角度及邊長都可以任意拉扯的三角形，並且在拉的過程會有點像是3D立體的模樣。

![](https://i.imgur.com/zifmM8V.png)



而我設計重置畫步鍵按下後，就不能再undo了，就是一個嶄新的畫面。

其餘的應該都跟助教要求相同，而會在下方排一排顏色的選項是因為本來不知道不能只是選固定的幾個顏色，又覺得刪掉自己精心挑選的顏色很可惜，所以就留下來當裝飾了，而且也確實能用喔！
有白色圈圈框住的顏色就代表是現在被選中的顏色。

![](https://i.imgur.com/dfZ4XG9.png)


### Gitlab page link

https://106010010.gitlab.io/AS_01_WebCanvas/

### Others (Optional)

助教辛苦了，謝謝:)

<style>
table th{
    width: 100%;
}
</style>