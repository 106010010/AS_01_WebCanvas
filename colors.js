var swatches = document.getElementsByClassName('swatch');

var strokeColor = document.getElementById("change_color");
var Color;

context.strokeStyle = strokeColor.value;
strokeColor.addEventListener("input", change_color, false);

for(var i=0, n=swatches.length; i<n; i++){
    swatches[i].addEventListener('click', setSwatch);
}

function setColor(color){
    context.fillStyle = color;
    context.strokeStyle = color;
    var active = document.getElementsByClassName('active')[0];
    if(active){
        active.className = 'swatch';
    }
    Color = color;
}

function setSwatch(e){
    var swatch = e.target;
    setColor(swatch.style.backgroundColor);
    swatch.className += ' active';
}

function change_color(){
    context.fillStyle = this.value;
    context.strokeStyle = this.value;
    event.stopPropagation();
}


