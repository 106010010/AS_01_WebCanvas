function setR(newR){
    if(newR < minR){
        newR = minR;
    }else if(newR > maxR){
        newR = maxR;
    }
    radius = newR;
    context.lineWidth = radius*2;

    Rspan.innerHTML = radius;
}

var minR = 5;
var maxR = 100;
var defaultR = 10;
var Rinterval = 5;
var Rspan = document.getElementById('Rval');
var decR = document.getElementById('decreaseR');
var incR = document.getElementById('increaseR');

decR.addEventListener('click', function(){
    setR(radius-Rinterval)
});

incR.addEventListener('click', function(){
    setR(radius+Rinterval)
});

setR(defaultR);